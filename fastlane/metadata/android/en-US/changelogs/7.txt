What's new and improved:

* [FIX] Fixed a redirection failure for twitter.com links to Nitter
* [CHR] Reflected changes to upstream's instance list that will launch in the application

